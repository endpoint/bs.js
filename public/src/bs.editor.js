/**
 * Adds support for json editing stuff
 *
 * tinyEditor is included by default but to reduce intiatal load you must call initJsonEditor to load it
 */

// <link rel="stylesheet" href="./lib/jsonEditor/jsoneditor.css">
// <script src="./lib/jsonEditor/jsoneditor.js"></script>
bs.initJsonEditor = (cb) => {


    return new Promise(async (resolve, reject) => {

        bs.loadCSS('./lib/jsonEditor/jsoneditor.css');
        bs.loadJS('./lib/jsonEditor/jsoneditor.js', () => {
            resolve();
            cb();
        })
        // bs.loadJS('../js/')
    })
}

bs.testJsonEditor = () => {
    if (typeof JSONEditor == "undefined") {
        bs.initJsonEditor(bs.testJsonEditor);
        return;
    }

    let dom = document.createElement('div');
    //`<div id="jsoneditor" style="width: 50vw; height: 500px;"></div>`)
    dom.id = 'jsoneditor_' + bs.id++;
    dom.style.width = '98%';
    dom.style.height = '100%';
    dom.style.padding = '8px'

    // const container = document.getElementById("jsoneditor")
    const options = {
        mode: 'tree',
        modes: ['code', 'form', 'text', 'tree', 'view', 'preview'], // allowed modes
        onModeChange: function (newMode, oldMode) {
            console.log('Mode switched from', oldMode, 'to', newMode)
        }
    }

    var editor
    bs.createPanel("Edit JSON", dom, {
        panelSize: bs.getGoodPanelSize(),
        callback: panel => {

            let container = panel.querySelector('#' + dom.id);

            editor = new JSONEditor(container, options);
            // set json
            const initialJson = {
                "Array": [1, 2, 3],
                "Boolean": true,
                "Null": null,
                "Number": 123,
                "Object": {"a": "b", "c": "d"},
                "String": "Hello World"
            }
            editor.set(initialJson)

            // get json
            const updatedJson = editor.get()
        },
        onclosed: panel => {
            const updatedJson = editor.get()
            console.log(updatedJson);
        }
    })
    // document.body.appendChild(dom[0]);
    // create the editor


}

/**
 * Create an iframe with a link. if the link ends in pdf / otd / supported format maybe it loads the associated viewer
 * @param link
 * @param options - extra config for what to do by default returns dom element
 * @param options.jspanel - jspanel options leave content blank since iframe is set, but you may yse the toolbars :) https://jspanel.de
 * @param cb - callback depends on what options you pick but generally will be when panel is loaded.
 */
bs.createIframe = (link, options, cb) => {
    options = options || {};
    let dom = document.createElement('iframe');
    // height: 98%; width: 100%; padding: 8px;
    dom.style.height = '98%'
    dom.style.width = '100%'
    dom.style.padding = '8px'
    dom.src = link;
    if (options.jspanel) {
        if (!bs.isObject(options.jspanel)) {
            options.jspanel = {};
        }
        let jspanelOptions = Object.assign({
            headerTitle: 'View Resource: ' + link,
            content: dom,
            panelSize: bs.getGoodPanelSize(1, 600)
        }, options.jspanel)

        jsPanel.create(jspanelOptions, (panel) => {
            dom.panel = panel;
        })

    }

    return dom;

}

bs.viewPDF = (link, title) => {

    bs.createIframe(bs.resorceRoot + 'lib/pdfjs/web/viewer.html?file=' + link, {
        jspanel: {
            panelSize: bs.getGoodPanelSize(),
            headerTitle: title || "View: " + link,
        }
    })
}

bs.testPdf = () => {
    bs.viewPDF(bs.resorceRoot + 'res/Graph-visualization-WP-compressed.pdf')
}

/**=========================
 * Doc Link
 ===========================*/
bs.showVideo = function (url, title, options) {
    title = title || url.split('/').pop().split('.')[0] // bar from foo/bar.mp4
    let w = options.panelSize.width || 480;
    let h = options.panelSize.width || 320;
    bs.createPanel(title,
        `
            <video width="${w}" height="${h}" autoplay controls>
              <source src="${url}" type="video/mp4">
              Your browser does not support the video tag.
            </video>        
        `,
        bs.mergeDeep({
            callback: (panel) => {
                console.log("Created panel: ", panel);
            },
            panelSize: {
                width: w + 10,
                height: h + 50,
            },
        }, options));
}

bs.showIFrame = function (url, title, options) {
    title = title || url.split('/').pop().split('.')[0] // bar from foo/bar.mp4
    let w = 480;
    let h = 320;
    bs.createPanel(title,
        `
            <iframe src="${url}" style="height: 98%; width: 100%; padding: 8px;">
            </iframe>        
        `,
        bs.mergeDeep({
            callback: (panel) => {
                console.log("Created panel: ", panel);
            },
            panelSize: {
                width: w + 10,
                height: h + 50,
            },
        }, options));
}

bs.createPanel = (title, content, options) => {

    let defaults = {
        theme: '#b9e8cc',
        borderRadius: '0.5rem',
        panelSize: bs.getGoodPanelSize(),
        headerTitle: title,
        content: content
    }

    let opts = bs.mergeDeep(defaults, options)
    return jsPanel.create(opts);
}

bs.openLinkModal = (link, nice_name) => {

    nice_name = nice_name || `Reference Document: ` + link;
    let fileurl = link;//ie bs.routeToURL('/api/files/downloadFile?path=' + link)

    // $('#fileViewModalTitle').html(nice_name);

    // let fileViewModalBody = $('#fileViewModalBody');

    // fileViewModalBody.html(`<canvas id="the-canvas"></canvas>`);

    // let fileViewModal = $('#fileViewModal');
    // fileViewModal.modal('show');

    let type = link.split('.').pop();

    if (type == 'pdf') {
        let viewerurl = bs.routeToURL(`/libs/pdfjs/web/viewer.html`);
        viewerurl += `?file=${encodeURIComponent(fileurl)}`;
        // let w = document.body.clientWidth / 2
        // let h = w * 11/8.5
        bs.createIframe(viewerurl, {
            headerTitle: nice_name,
            panelSize: bs.getGoodPanelSize(8.5 / 11),
            position: {
                my: 'right-bottom',
                at: 'right-bottom',
                of: 'body',
                offsetX: -50,
                offsetY: -15
            },
            dragit: {
                containment: 0,
                snap: {
                    sensitivity: 70,
                    trigger: 'panel',
                    active: 'both',
                },
            },
        });
    } else if (type == 'mp4') {

        // let w = document.body.clientWidth / 2
        // let h = w * 9/16
        bs.showVideo(fileurl, nice_name, {
            panelSize: bs.getGoodPanelSize(16 / 9),
            position: {
                my: 'right-bottom',
                at: 'right-bottom',
                of: 'body',
                offsetX: -50,
                offsetY: -15
            },
            dragit: {
                containment: 0,
                snap: {
                    sensitivity: 70,
                    trigger: 'panel',
                    active: 'both',
                },
            },
        });
    }

    return;

    // let viewerurl = bs.routeToURL(`/libs/pdfjs/web/viewer.html`);
    let viewerurl = bs.resorceRoot + `libs/pdfjs/web/viewer.html`;

    viewerurl += `?file=${encodeURIComponent(fileurl)}`;


    // show file
    // fileViewModalBody.html(`
    //     <iframe class="file-iframe " src="${url}"></${tag}>
    // `);
    //

    bs.createIframe(viewerurl)
    try {
        let tag = 'iframe';
        fileViewModalBody.html(`
                                <${tag} class="file-iframe" src="${viewerurl}"></${tag}>
                             `);

    } catch (e) {
        if (fileViewModal.hasClass('show')) {
            console.error(e);
        } else {
            console.warn('Ignoring error because modal closed');
        }
    }

}

//todo finish
bs.docLinks = (links) => {


    if (!Array.isArray(links)) {
        console.error("Must pass an array of links or link objects (url, text,)");

    }

    // window._openLinkModal =
    function docFormatter(row, cell, value, columnDef, dataContext) {

        if (typeof this._actions == "undefined") {
            this._actions = [];
        }

        let html = `<a onclick="_openLinkModal('${dataContext.path}', '${dataContext.nice_name}');">${dataContext.nice_name}</a>`;
        console.log("DOC FORMATER: ", html)
        return html;

    }

    console.log("Links:", links);

    let height = 100 + 25 * links.length;
    if (height > 400) height = 400;
    window.docGrid = new Grid(
        links,
        [{
            id: 'documents',
            name: 'Documents',
            field: 'nice_name',
            width: 275,
            maxWidth: 400,
            minWidth: 270,
            formatter: docFormatter,
        }],
        {
            jspanelOptions: {
                headerTitle: 'Documents',
                panelSize: {
                    width: 280,
                    height: height,
                },
                position: {
                    my: 'left-top',
                    at: 'left-bottom',
                    of: 'span.state-open-docserver',
                    offsetX: -10,
                    offsetY: 15
                }
            },


            // autosizeColsMode: Slick.GridAutosizeColsMode.LegacyForceFit,
            // autosizeTextAvgToMWidthRatio: 1,
            // autosizeColPaddingPx: 8,
            // viewportSwitchToScrollModeWidthPercent: 120,
        }
    )

    // docGrid.show();
}

