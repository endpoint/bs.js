// Project configuration.
//this will use our configs and generate the list of files to be bundled
// ask kier to explain this if you have issues grunt can be weird


module.exports = (grunt) => {

    //this import all the user dependencies we have in our package.json
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    // grunt.loadNpmTasks('grunt-markdown');
    // grunt.loadNpmTasks('grunt-concurrent');
    // grunt.registerTask('default', ['uglify', 'cssmin']);


    console.log('Woo lets build some code 🎉💃', grunt);

    //############################################################
    // General Libs / Distribution Files almost all '/'s matter !!
    //############################################################
    const DEST = '/public/'
    const JSDEST = DEST + 'js/'
    const CSSDEST = DEST + 'css/'
    const DOCDEST = DEST + 'doc/'
    /**
     * Define code bundles like so and then call them with
     * grunt bundle:name
     * @type {{"name": {css: string[], options: {}, js: string[]}}}
     * Outputs : $CSSDEST$name.min.css
     *           $JSDEST$name.js / .min.js / .min.js.map
     *           /public/doc/index.html
     */
    let bundles = {
        'backbone': {
            js: ['./node_modules/backbone/backbone.js'],
        },
        // 'jquery': {
        //     js: ['./node_modules/jquery/dist/jquery.slim.js'],
        // },
        'jspanel.all': {
            js: [//test
                <!-- loading jsPanel javascript -->
                "./node_modules/jspanel4/dist/jspanel.js",
                <!-- optionally load jsPanel extensions -->
                "./node_modules/jspanel4/dist/extensions/contextmenu/jspanel.contextmenu.js",
                "./node_modules/jspanel4/dist/extensions/hint/jspanel.hint.js",
                "./node_modules/jspanel4/dist/extensions/modal/jspanel.modal.js",
                "./node_modules/jspanel4/dist/extensions/tooltip/jspanel.tooltip.js",
                "./node_modules/jspanel4/dist/extensions/dock/jspanel.dock.js",
                "./node_modules/jspanel4/dist/extensions/dialog/jspanel.dialog.js",
                "./node_modules/jspanel4/dist/extensions/layout/jspanel.layout.js",
                "./node_modules/jspanel4/dist/extensions/datepicker/jspanel.datepicker.js",

            ],
            css: [
                "./public/css/animate.css",
                "./public/css/fa-all.css",
                "./node_modules/jspanel4/dist/jspanel.css",
                "./node_modules/jspanel4/dist/extensions/dialog/jspanel.dialog.css",
                "./node_modules/jspanel4/dist/extensions/datepicker/theme/default.css",

            ],
            options: {
                jsdoc: true,
            }
        },
        'idog_node': {// gun no unix, idog_node.js
            //                  idog_node.min.js / indeterminate distributed open graph node
            js: [
                // './public/js/bs.js',
                './node_modules/gun/sea.js',
                './node_modules/gun/gun.js',
                './node_modules/gun/nts.js',
                // './public/src/bs.vanilla.js',
                // './public/src/bs.qr.js',
                // './public/src/bs.panel.js',
                // './public/src/bs.bootstrap.js',
                // './public/src/bs.lazy.js'
            ],
            options: {
                jsdoc: true,
            }
        },
        // 'jsonEditor': {// just copied from bellow
        //     js: [
        //         './node_modules/jsoneditor/dist/jsoneditor.js',
        //         // './public/src/bs.vanilla.js',
        //         // './public/src/bs.qr.js',
        //         // './public/src/bs.panel.js',
        //         // './public/src/bs.bootstrap.js',
        //         // './public/src/bs.lazy.js'
        //     ],
        //     css: ['./node_modules/jsoneditor/dist/jsoneditor.css'],
        //     options: {
        //         jsdoc: false,
        //     }
        // },
        'bs_only': {
            js: [
                './public/src/bs.js',
                './public/src/bs.vanilla.js',
                './public/src/bs.fastFrag.js',
                './public/src/bs.editor.js',
                './public/src/bs.pts.js',
                './public/src/bs.qr.js',
                // './public/src/bs.panel.js',
                // './public/src/bs.bootstrap.js',
                // './public/src/bs.lazy.js'
            ],
            css: [
            ],
            options: {
                jsdoc: true,
            }
        },
        // 'bootstrap': {
        //     js: [
        //         './public/js/bs.js',
        //
        //
        //         // './public/src/bs.vanilla.js',
        //         // './public/src/bs.qr.js',
        //         // './public/src/bs.panel.js',
        //         // './public/src/bs.bootstrap.js',
        //         // './public/src/bs.lazy.js'
        //     ],
        //     options: {
        //         jsdoc: true,
        //     }
        // },
        'all': {
            js: [

                // './public/js/jquery.min.js',


                './public/js/jspanel.all.js',


                './public/js/bs_only.js',
                // './public/js/backbone.min.js',
                './public/js/idog.js',
                './node_modules/tiny-editor/dist/bundle.js',

                // './node_modules/gun/sea.js',
                // './node_modules/gun/gun.js',
                // './node_modules/gun/nts.js',
            ],
            css: [
                "./public/css/animate.css",
                './public/css/css.gg.css',
                "./public/css/fa-all.css",
                './public/css/water.css',
                './public/css/jspanel.all.min.css',
                // './node_modules/tiny-editor/dist/'
                './public/css/tinyEditor.css',

                // "./node_modules/jspanel4/dist/jspanel.css",
                // "./node_modules/jspanel4/dist/extensions/dialog/jspanel.dialog.css",
                // "./node_modules/jspanel4/dist/extensions/datepicker/theme/default.css",
                './public/css/bs.css',

            ],
            options: {
                jsdoc: false
            }
        }
    }

    //globals for grunt initConfig
    let terser = {};
    let cssmin = {};
    let jsdoc = {};
    let browserify = {};
    let watch = {};
    let devBundleTasks = [];
    let devWatchTasks = [];
    let devConcurrentTasks = [];
    let allTasks = [];
    let allsrcfiles = []

    /**
     * Todo add functinality to verify intagrity
     *
     * @param entry
     * @return {*[]}
     */
    function generateBuildConfig(entry) {
        let name = entry[0]
        let config = entry[1]; // {js, css, options}
        config.options = config.options || {}
        config.js = config.js || []
        config.css = config.css || []
        //dest directorys
        let dest = '.' + DEST
        let jsdest = '.' + JSDEST + name + '.js'
        let jsdest_prod = '.' + JSDEST + name + '.min.js'
        let cssdest = '.' + CSSDEST + name + '.min.css'


        if (config.options.jsdoc) {
            allsrcfiles = [...allsrcfiles, ...config.js]
        }
        let jsfiles = Object.fromEntries([[jsdest, config.js]]);
        let jsfiles_prod = Object.fromEntries([[jsdest_prod, config.js]]);
        let cssfiles = Object.fromEntries([[cssdest, config.css]]);

        let tasks = [];

        let terser_config = {
            options: {
                mangle: false,
                compress: false,
                sourceMap: {
                    includeSources: true,
                    url: `./${name}.js.map`
                }
                // Task-specific options go here.
            },
            files: jsfiles //this will use our configs and generate the list of files to be bundled
        }
        let terser_min_config = {
            options: {
                //     mangle: true,
                // // compress: true,
                // compress: {
                //     drop_console: true
                // }
                mangle: true,
                compress: true,
                sourceMap: {
                    includeSources: true,
                    url: `./${name}.min.js.map`
                }
                // Task-specific options go here.
            },
            files: jsfiles_prod //this will use our configs and generate the list of files to be bundled
        }
        let cssmin_config = {
            options: {
                report: 'gzip',
                keepSpecialComments: 0,
                sourceMap: true,
                outputSourceFiles: true,
                banner: 'This is the core js complement'
            },
            files: cssfiles

        }

        // let name = out.split('/').pop().split('.').shift() // /whatever/path/name.min.js; // extract name


        console.log('Preparing to bundle terse:', name, jsfiles);
        let devKey = name;
        let prodKey = name + '.min';

        terser[devKey] = terser_config;
        terser[prodKey] = terser_min_config;
        cssmin[name] = cssmin_config;


        //write configs
        // devBundleTasks.push('terser:' + devKey);
        // devWatchTasks.push('watch:' + devKey);
        // devConcurrentTasks.push(['terser:' + devKey, 'watch:' + devKey]);

        //watch task to rebuild js
        watch[devKey] = {
            files: jsfiles,
            tasks: ['terser:' + devKey]
        }
        let bundle_tasks = []
        if (config.css.length > 0) bundle_tasks.push('cssmin:' + name)
        if (config.js.length > 0) {
            bundle_tasks.push('terser:' + devKey)
            bundle_tasks.push('terser:' + prodKey)
        }

        // ['terser:' + devKey, 'terser:' + prodKey, , /*'jsdoc:' + devKey*/]
        return bundle_tasks


    }

    //build all tasks from our bundels config object
    let tasks = Object.fromEntries(Object.entries(bundles).map(
        (e) => {
            let tasks = generateBuildConfig(e)
            // an array of all grunt tasks you can nonw execut ie [terser:jspanel, cssmin:jspanel, jsdoc:jspanel, ]
            // grunt.registerTask('build:'+e[0], tasks );

            return [e[0], tasks]
        }));

    //then we initalize grunt
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        //this is the process we are using
        // browserify is the first step if necessary. If you are using
        // a project that only gives modern files or want to write fancy new code
        // use this .. planmap only officially supports modern browsers anyway
        // browserify: {
        //     // "parser": "babel-eslint",
        //     // build: {
        //     //     src: './build/browserify/config_loader.js',
        //     //     dest: './build/config.js'
        //     // },
        //         ...browserify,
        //     // easyreport: {//todo name this better
        //     //
        //     //     files: {
        //     //         // destination for transpiled js : source js
        //     //         './build/browserify/EasyReport.js': './src/cjs/EasyReport.js',
        //     //         './build/browserify/PhotoSphereViewer.js': './src/cjs/PhotoSphereViewer.js',
        //     //         './build/browserify/d3.js': './src/cjs/d3.js'
        //     //     },
        //     //     options: {
        //     //         transform: [['babelify', {
        //     //             presets: ["@babel/preset-env"],
        //     //             // presets: ["es2015"],
        //     //             plugins: [
        //     //                 ["@babel/transform-runtime"]
        //     //             ],
        //     //             sourceMaps: true,
        //     //             global: true,
        //     //             // ignore: [/\/node_modules\/(?!your module folder\/)/]
        //     //         }]],
        //     //         browserifyOptions: {
        //     //             sourceMaps: true,
        //     //             global: true,
        //     //             "sourceType": "module",
        //     //             "allowImportExportEverywhere": true,
        //     //             debug: true
        //     //         }
        //     //     }
        //     // }
        // },

        terser: {
            ...terser
        },

        jsdoc: {
            dist: {
                src: allsrcfiles,
                // "tags": {
                //     "allowUnknownTags": true
                // },
                // "plugins": ["plugins/markdown"],
                // "markdown.hardwrap": true,
                // "markdown": {
                //     "hardwrap": true
                // },
                options: {

                    recurse: true,
                    verbose: true,
                    package: 'package.json',
                    readme: 'README.md',
                    // private: true,
                    // tutorials: 'tutorials',
                    // force: true,
                    destination: '.' + DOCDEST,
                    // "plugins": ["jsdoc-mermaid"],
                    // "template": "node_modules/docdash",
                    "template": "./node_modules/clean-jsdoc-theme",
                    "theme_opts": {
                        "default_theme": "light",
                        includeFilesListInHomepage: true,
                        displayModuleHeader: true,
                        title: "bsjs docs"

                    }
                }
            }
        },

        cssmin: {
            ...cssmin, // expand all configured bundles into cssmin
            public: {
                files: [{
                    expand: true,
                    cwd: 'public/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'public/css',
                    ext: '.min.css'
                }]
            },
        },

        copy: {
            jsonEditor: {
                files: [
                    {
                        cwd: './node_modules/jsoneditor/dist/',  // set working folder / root to copy
                        src: '**/*',           // copy all files and subfolders
                        dest: './public/lib/jsonEditor/',
                        expand: true
                    }
                ]
            }
        }
    });


    //now we register our master tasks using all our bundles
    let deployTasks = []
    for (let name in tasks) {

        grunt.registerTask('build:' + name, tasks[name]);

        deployTasks.push('build:' + name)
    }

    deployTasks.push('copy:jsonEditor');

    deployTasks.push('jsdoc');

    deployTasks.push('cssmin:public');

    console.log(deployTasks);
    grunt.registerTask('deploy', deployTasks)
}
