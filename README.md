# bs.js
[![Netlify Status](https://api.netlify.com/api/v1/badges/c6ee751e-ecff-4bfa-a915-8c0c857ec405/deploy-status)](https://app.netlify.com/sites/bsjs/deploys)


Hosted by cloudflair at https://bsjs.sgol.pub


jquery
jspanel
animate.css
css.gg
qr



This is the base utility that all makes programing in the browser a breeze

I often use jquery and bootstrap but bs.js should be mostly vanilla

TODO make stable deploy

## Getting started

https://bsjs.netlify.app

https://gitlab.com/endpoint/bs.js
```
wget https://bsjs.sgol.pub/js/all.min.css
all.min.css  1.93K  --.-KB/s    in 0s 
wget https://bsjs.sgol.pub/js/all.min.js
all.min.js   265.50K   962KB/s    in 0.3s
#todo update stats
```
```
cd parent_dir
git clone https://gitlab.com/endpoint/bs.js.git
cd bs.js
npm install
npm run build
```




## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/endpoint/bs.js/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.


mango@mango:~/git/bs.js$ ls -sh public/js/
total 14M
252K all.js            24K backbone.min.js      240K bs_only.min.js.map    384K jquery.js.map      3.0M jsonEditor.min.js.map
504K all.js.map       120K backbone.min.js.map   92K idog_node.js           72K jquery.min.js      180K jspanel.all.js
196K all.min.js       144K bs.js                308K idog_node.js.map      372K jquery.min.js.map  484K jspanel.all.js.map
488K all.min.js.map    68K bs_only.js            72K idog_node.min.js      1.3M jsonEditor.js      136K jspanel.all.min.js
 36K backbone.js      244K bs_only.js.map       292K idog_node.min.js.map  3.1M jsonEditor.js.map  468K jspanel.all.min.js.map
120K backbone.js.map   56K bs_only.min.js       116K jquery.js             936K jsonEditor.min.js
